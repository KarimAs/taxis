/**
 * Un acteur dans la simulation de la compagnie de taxi.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public interface Actor
{
    /**
     * Met en oeuvre le comportement d'un acteur.
     */
    public void act();
}
